<?php


/**
 * Compile la balise `#SVPSKEL_BRANCHES_SPIP`
 *
 * Cette balise retourne une liste des branches de SPIP
 *
 * Avec un paramètre indiquant une branche, la balise retourne
 * une liste des bornes mini et maxi de cette branche.
 *
 * @example
 *     #SVPSKEL_BRANCHES_SPIP       : array('1.9', '2.0', '2.1', ....)
 *     #SVPSKEL_BRANCHES_SPIP{3.0}  : array('3.0.0', '3.0.999')
 *
 * @balise
 * @see calcul_SVPSKEL_BRANCHES_SPIP()
 *
 * @param Champ $p
 *     Pile au niveau de la balise
 * @return Champ
 *     Pile complétée par le code à générer
 **/
function balise_SVPSKEL_BRANCHES_SPIP($p) {
	// nom d'une branche en premier argument
	if (!$branche = interprete_argument_balise(1, $p)) {
		$branche = "''";
	}
	$p->code = 'calcul_SVPSKEL_BRANCHES_SPIP(' . $branche . ')';

	return $p;
}

/**
 * Retourne une liste des branches de SPIP, ou les bornes mini et maxi
 * d'une branche donnée
 *
 * @param string $branche
 *     Branche dont on veut récupérer les bornes mini et maxi
 * @return array
 *     Liste des branches array('1.9', '2.0', '2.1', ....)
 *     ou liste mini et maxi d'une branche array('3.0.0', '3.0.999')
 **/
function calcul_SVPSKEL_BRANCHES_SPIP($branche) {

	$retour = array();

	// pour $GLOBALS['infos_branches_spip']
	include_spip('inc/svp_outiller');
	$svp_branches = $GLOBALS['infos_branches_spip'];

	if (is_array($svp_branches)) {
		if (($branche) and in_array($branche, $svp_branches)) {
			// On renvoie les bornes inf et sup de la branche specifiee
			$retour = $svp_branches[$branche];
		} else {
			// On renvoie uniquement les numeros de branches
			$retour = array_keys($svp_branches);
		}
	}

	return $retour;
}


/**
 * Retourne un texte HTML présentant des statistiques d'un dépot
 *
 * Liste le nombre de plugins et de paquets d'un dépot
 * Indique aussi le nombre de dépots si l'on ne demande pas de dépot particulier.
 *
 * @uses svpskel_compter()
 * @param int $id_depot
 *     Identifiant du dépot
 * @return string
 *     Code HTML présentant les statistiques du dépot
 **/
function svpskel_afficher_statistiques_globales($id_depot = 0) {
	$info = '';

	$total = svpskel_compter('depot', $id_depot);
	if (!$id_depot) {
		// Si on filtre pas sur un depot alors on affiche le nombre de depots
		$info = '<li id="stats-depot" class="item">
					<div class="unit size4of5">' . ucfirst(trim(_T('svp:info_depots_disponibles', array('total_depots' => '')))) . '</div>
					<div class="unit size1of5 lastUnit">' . $total['depot'] . '</div>
				</li>';
	}
	// Compteur des plugins filtre ou pas par depot
	$info .= '<li id="stats-plugin" class="item">
				<div class="unit size4of5">' . ucfirst(trim(_T('svp:info_plugins_heberges', array('total_plugins' => '')))) . '</div>
				<div class="unit size1of5 lastUnit">' . $total['plugin'] . '</div>
			</li>';
	// Compteur des paquets filtre ou pas par depot
	$info .= '<li id="stats-paquet" class="item">
				<div class="unit size4of5">' . ucfirst(trim(_T('svp:info_paquets_disponibles', array('total_paquets' => '')))) . '</div>
				<div class="unit size1of5 lastUnit">' . $total['paquet'] . '</div>
			</li>';

	return $info;
}


/**
 * Retourne un texte indiquant un nombre total de paquets
 *
 * Calcule le nombre de paquets correspondant à certaines contraintes,
 * tel que l'appartenance à un certain dépot, une certaine catégorie
 * ou une certaine branche de SPIP et retourne une phrase traduite
 * tel que «78 paquets disponibles»
 *
 * @uses svpskel_compter()
 * @param int $id_depot
 *     Identifiant du dépot
 *     Zéro (par défaut) signifie ici : «dans tous les dépots distants»
 *     (id_dépot>0) et non «dans le dépot local»
 * @param string $categorie
 *     Type de catégorie (auteur, communication, date...)
 * @param string $compatible_spip
 *     Numéro de branche de SPIP. (3.0, 2.1, ...)
 * @return string
 *     Texte indiquant un nombre total de paquets
 **/
function svpskel_compter_telechargements($id_depot = 0, $categorie = '', $compatible_spip = '') {
	$total = svpskel_compter('paquet', $id_depot, $categorie, $compatible_spip);
	$info = _T('svp:info_paquets_disponibles', array('total_paquets' => $total['paquet']));

	return $info;
}



/**
 * Retourne un texte indiquant un nombre total de contributions pour un dépot
 *
 * Calcule différents totaux pour un dépot donné et retourne un texte
 * de ces différents totaux. Les totaux correspondent par défaut aux
 * plugins et paquets, mais l'on peut demander le total des autres contributions
 * avec le second paramètre.
 *
 * @uses svpskel_compter()
 * @param int $id_depot
 *     Identifiant du dépot
 *     Zéro (par défaut) signifie ici : «dans tous les dépots distants»
 *     (id_dépot>0) et non «dans le dépot local»
 * @param string $contrib
 *     Type de total demandé ('plugin' ou autre)
 *     Si 'plugin' : indique le nombre de plugins et de paquets du dépot
 *     Si autre chose : indique le nombre des autres contributions, c'est
 *     à dire des zips qui ne sont pas des plugins, comme certaines libraires ou
 *     certains jeux de squelettes.
 * @return string
 *     Texte indiquant certains totaux tel que nombre de plugins, nombre de paquets...
 **/
function svpskel_compter_depots($id_depot, $contrib = 'plugin') {

	$total = svpskel_compter('depot', $id_depot);
	if (!$id_depot) {
		$info = _T('svp:info_depots_disponibles', array('total_depots' => $total['depot'])) . ', ' .
			_T('svp:info_plugins_heberges', array('total_plugins' => $total['plugin'])) . ', ' .
			_T('svp:info_paquets_disponibles', array('total_paquets' => $total['paquet']));
	} else {
		if ($contrib == 'plugin') {
			$info = _T('svp:info_plugins_heberges', array('total_plugins' => $total['plugin'])) . ', ' .
				_T('svp:info_paquets_disponibles', array('total_paquets' => $total['paquet'] - $total['autre']));
		} else {
			$info = _T('svp:info_contributions_hebergees', array('total_autres' => $total['autre']));
		}
	}

	return $info;
}



/**
 * Retourne un texte indiquant un nombre total de plugins
 *
 * Calcule le nombre de plugins correspondant à certaines contraintes,
 * tel que l'appartenance à un certain dépot, une certaine catégorie
 * ou une certaine branche de SPIP et retourne une phrase traduite
 * tel que «64 plugins disponibles»
 *
 * @uses svpskel_compter()
 * @param int $id_depot
 *     Identifiant du dépot
 *     Zéro (par défaut) signifie ici : «dans tous les dépots distants»
 *     (id_dépot>0) et non «dans le dépot local»
 * @param string $categorie
 *     Type de catégorie (auteur, communication, date...)
 * @param string $compatible_spip
 *     Numéro de branche de SPIP. (3.0, 2.1, ...)
 * @return string
 *     Texte indiquant un nombre total de paquets
 **/
function svpskel_compter_plugins($id_depot = 0, $categorie = '', $compatible_spip = '') {
	$total = svpskel_compter('plugin', $id_depot, $categorie, $compatible_spip);
	$info = _T('svp:info_plugins_disponibles', array('total_plugins' => $total['plugin']));

	return $info;
}



/**
 * Compte le nombre de plugins, paquets ou autres contributions
 * en fonction de l'entité demandée et de contraintes
 *
 * Calcule, pour un type d'entité demandé (depot, plugin, paquet, catégorie)
 * leur nombre en fonction de certaines contraintes, tel que l'appartenance
 * à un certain dépot, une certaine catégorie ou une certaine branche de SPIP.
 *
 * Lorsque l'entité demandée est un dépot, le tableau des totaux possède,
 * en plus du nombre de dépots, le nombre de plugins et paquets.
 *
 * @note
 *     Attention le critère de compatibilite SPIP pris en compte est uniquement
 *     celui d'une branche SPIP
 *
 * @param string $entite
 *     De quoi veut-on obtenir des comptes. Peut être 'depot', 'plugin',
 *    'paquet' ou 'categorie'
 * @param int $id_depot
 *     Identifiant du dépot
 *     Zéro (par défaut) signifie ici : «dans tous les dépots distants»
 *     (id_dépot>0) et non «dans le dépot local»
 * @param string $categorie
 *     Type de catégorie (auteur, communication, date...)
 * @param string $compatible_spip
 *     Numéro de branche de SPIP. (3.0, 2.1, ...)
 * @return array
 *     Couples (entite => nombre).
 **/
function svpskel_compter($entite, $id_depot = 0, $categorie = '', $compatible_spip = '') {
	$compteurs = array();

	$where = array();
	if ($id_depot) {
		$where[] = "t1.id_depot=" . sql_quote($id_depot);
	} else {
		$where[] = "t1.id_depot>0";
	}

	if ($entite == 'plugin') {
		$from = array('spip_plugins AS t2', 'spip_depots_plugins AS t1');
		$where[] = "t1.id_plugin=t2.id_plugin";
		if ($categorie) {
			$where[] = "t2.categorie=" . sql_quote($categorie);
		}
		if ($compatible_spip) {
			$creer_where = charger_fonction('where_compatible_spip', 'inc');
			$where[] = $creer_where($compatible_spip, 't2', '>');
		}
		$compteurs['plugin'] = sql_count(sql_select('DISTINCT t1.id_plugin', $from, $where));
	} elseif ($entite == 'paquet') {
		$from = array('spip_paquets AS t1');
		if ($categorie) {
			$ids = sql_allfetsel('id_plugin', 'spip_plugins', 'categorie=' . sql_quote($categorie));
			$ids = array_column($ids, 'id_plugin');
			$where[] = sql_in('t1.id_plugin', $ids);
		}
		if ($compatible_spip) {
			$creer_where = charger_fonction('where_compatible_spip', 'inc');
			$where[] = $creer_where($compatible_spip, 't1', '>');
		}
		$compteurs['paquet'] = sql_countsel($from, $where);
	} elseif ($entite == 'depot') {
		$from = array('spip_depots AS t1');
		$select = array(
			'COUNT(t1.id_depot) AS depot',
			'SUM(t1.nbr_plugins) AS plugin',
			'SUM(t1.nbr_paquets) AS paquet',
			'SUM(t1.nbr_autres) AS autre'
		);
		$compteurs = sql_fetsel($select, $from, $where);
	} elseif ($entite == 'categorie') {
		$from = array('spip_plugins AS t2', 'spip_depots_plugins AS t1');
		$where[] = "t1.id_plugin=t2.id_plugin";
		if ($id_depot) {
			$ids = sql_allfetsel('id_plugin', 'spip_depots_plugins AS t1', $where);
			$ids = array_column($ids, 'id_plugin');
			$where[] = sql_in('t2.id_plugin', $ids);
		}
		if ($compatible_spip) {
			$creer_where = charger_fonction('where_compatible_spip', 'inc');
			$where[] = $creer_where($compatible_spip, 't2', '>');
		}
		if ($categorie) {
			$where[] = "t2.categorie=" . sql_quote($categorie);
			$compteurs['categorie'] = sql_count(sql_select('DISTINCT t1.id_plugin', $from, $where));
		} else {
			$select = array('t2.categorie', 'count(DISTINCT t1.id_plugin) as nb');
			$group_by = array('t2.categorie');
			$plugins = sql_allfetsel($select, $from, $where, $group_by);
			$compteurs['categorie'] = array_column($plugins, 'nb', 'categorie');
		}
	}

	return $compteurs;
}
